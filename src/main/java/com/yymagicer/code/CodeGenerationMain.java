package com.yymagicer.code;

import cn.hutool.core.util.StrUtil;
import com.yymagicer.code.entity.CodeContext;
import com.yymagicer.code.entity.ColumnInfo;
import com.yymagicer.code.entity.PathInfo;
import com.yymagicer.code.entity.TableInfo;
import com.yymagicer.code.service.BuildCode;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * TODO
 *
 * @author yymagicer
 * @version 1.0
 * @date 2022/1/19 上午12:14
 */
public class CodeGenerationMain {
    public static void main(String[] args) {

        //args = new String[]{"config.xml"};
        // 只生成针对一个表生成代码，为空表示生成所有代码
        String cusTableName = null;
        System.out.println("检查参数1：XML配置文件");
        if (args.length < 1 || args.length > 2) {
            System.out.println("参数错误");
            return;
        }
        if (args.length == 2) {
            cusTableName = args[1];
        }
        CodeContext codeContext = new CodeContext();
        codeContext.setTableInfoList(new ArrayList<>());
        PathInfo pathInfo = new PathInfo();
        codeContext.setPathInfo(pathInfo);

        System.out.println("解析文件：" + args[0]);
        String tableNamePrefixion = null;
        String tableScriptDir = null;
        String entityPackage = null;
        String daoPackage = null;
        String servicePackage = null;
        String serviceImpPackage= null;
        String codePath= null;
        String ftlFileDir= null;
        try {
            SAXReader saxReader = new SAXReader();
            Document doc = saxReader.read(new File(args[0]));
            Element zroot = doc.getRootElement();
            // TableNamePrefixion可以为空
            tableNamePrefixion = VNode(zroot, "TableNamePrefixion", false);
            tableScriptDir = VNode(zroot, "TableScriptDir");
            entityPackage = VNode(zroot, "EntityPackage");
            daoPackage = VNode(zroot, "DaoPackage");
            servicePackage = VNode(zroot, "ServicePackage");
            serviceImpPackage = VNode(zroot, "ServiceImpPackage");
            codePath = VNode(zroot, "CodePath");
            ftlFileDir = VNode(zroot, "FtlFileDir");

            pathInfo.setEntityPath(entityPackage);
            pathInfo.setMapperPath(daoPackage);
            pathInfo.setServicePath(servicePackage);
            pathInfo.setServiceImplPath(serviceImpPackage);
            pathInfo.setFtlPath(ftlFileDir);
            codeContext.setCodePath(codePath);

        } catch (Exception e) {
            System.out.println("发生错误：" + e.getMessage());
        }
        File[] fs = new File(tableScriptDir).listFiles();
        for (int i = 0; i < fs.length; i++) {
            if (fs[i].getName().matches(".+(?i)\\.sql$")) {
                System.out.println("分析数据库脚本：" + fs[i].getName());
                String tablestr = "";
                try {
                    // FileReader fr = new FileReader(fs[i]);
                    InputStreamReader fr = null;
                    try {
                        fr = new InputStreamReader(new FileInputStream(fs[i]), "UTF-8");
                    } catch (UnsupportedEncodingException | FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    BufferedReader br = new BufferedReader(fr);
                    try {
                        String rline = br.readLine();
                        while (rline != null) {
                            boolean canread = false;
                            StringBuilder sbf = new StringBuilder();
                            while (rline != null) {
                                if (rline.matches("(?i)\\s*CREATE\\s*TABLE\\s*`[^`]+`\\s*\\(.*")) {
                                    canread = true;
                                }
                                if (canread) {
                                    sbf.append(rline);
                                }
                                if (rline.matches(".*?\\).*?(?i)COMMENT\\s*=\\s*'[^']+'.*?;.*?$")) {
                                    rline = br.readLine();
                                    break;
                                }
                                rline = br.readLine();
                            }
                            tablestr = sbf.toString();
                            if (tablestr.matches(
                                    "(?i)\\s*CREATE\\s*TABLE\\s*`[^`]+`\\s*\\(.*\\).*COMMENT\\s*=\\s*'[^']+'.*;.*$")) {

                                process(codeContext, tablestr, cusTableName, tableNamePrefixion, entityPackage,
                                        daoPackage, servicePackage, serviceImpPackage, codePath);
                                BuildCode.buildEntity(codeContext);
                                BuildCode.buildMapper(codeContext);
                                BuildCode.buildXml(codeContext);
                            }
                            br.close();
                            fr.close();
                        }
                    } catch (FileNotFoundException | UnsupportedEncodingException e) {
                        e.printStackTrace();
                        System.err.println("发生错误：" + e.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void process(CodeContext codeContext,String tableSql, String cusTableName, String tableNamePrefixion, String entityPackage,
                                            String daoPackage, String servicePackage, String serviceImpPackage, String codePath)
            throws Exception {

        Matcher zm = Pattern
                .compile("\\s*CREATE\\s*TABLE\\s*`" + tableNamePrefixion
                        + "([^`]+)`\\s*\\((.*)\\).*COMMENT\\s*=\\s*'([^']+)'.*;.*", Pattern.CASE_INSENSITIVE)
                .matcher(tableSql);
        if (zm.find()) {
            String tableName = tableNamePrefixion+zm.group(1).trim().toLowerCase();
            String entityName =StrUtil.upperFirst(StrUtil.toCamelCase(zm.group(1).trim().toLowerCase()));
            String tableComment = zm.group(3).trim();
            if (cusTableName != null) {
                if (!cusTableName.equals(tableName)) {
                    System.out.println("找到数据表：" + tableName + " ，并非指定，不生成。");
                }
            }
            System.out.println("找到数据表：" + tableName);

            String tableCon = zm.group(2).trim();
            List<String> prilist = new ArrayList<String>();
            zm = Pattern.compile(".*PRIMARY\\s+KEY\\s*\\(([^\\(\\)]+)\\).*").matcher(tableCon);
            if (zm.find()) {
                zm = Pattern.compile("\\s*`\\s*([^`]+?)\\s*`\\s*").matcher(zm.group(1));
                while (zm.find()) {
                    prilist.add(zm.group(1).trim());
                }
            }


            TableInfo tableInfo =new TableInfo();
            codeContext.getTableInfoList().add(tableInfo);

            tableInfo.setTableName(tableName);
            tableInfo.setTableComment(tableComment);
            tableInfo.setEntityName(entityName);
            List<ColumnInfo> commonColumnInfoList = new ArrayList<>();
            List<ColumnInfo> allColumns = new ArrayList<>();
            tableInfo.setCommonColumns(commonColumnInfoList);
            tableInfo.setAllColumns(allColumns);

            ColumnInfo primaryColumn = null;

            zm = Pattern
                    .compile(
                            "\\s*`([^`]+)`\\s*([a-zA-Z]+?)(\\s+|\\s*\\([\\d\\s,]+\\))([^`]*?)COMMENT\\s*'([^`]+)'\\s*(?:,|\\)|$)")
                    .matcher(tableCon);
            while (zm.find()) {
                tableCon = zm.group(0).trim();
                ColumnInfo columnInfo = new ColumnInfo();
                columnInfo.setColumnName(zm.group(1).trim().toLowerCase());
                columnInfo.setColumnComment(zm.group(5).trim());
                columnInfo.setFieldName(StrUtil.lowerFirst(StrUtil.toCamelCase(zm.group(1).trim().toLowerCase())));
                columnInfo.setType(zm.group(2).trim().toLowerCase());
                allColumns.add(columnInfo);
                boolean isPri = false;
                for (String pstr : prilist) {
                    if (pstr.equalsIgnoreCase(columnInfo.getFieldName())) {
                        isPri = true;
                        break;
                    }
                }
                if (isPri) {
                    primaryColumn = columnInfo;
                }else{
                    commonColumnInfoList.add(columnInfo);
                }
                tableInfo.setPrimaryColumn(primaryColumn);
            }
        } else {
            System.err.println("发生错误：没有找到表数据！");
        }
    }



    /**
     * 检查配置节点是否为空，配置节点为目录的检查目录是否存在
     */
    private static String VNode(Element zroot, String NodeName) throws Exception {
        return VNode(zroot, NodeName, true);
    }

    /**
     * 检查配置节点是否为空，配置节点为目录的检查目录是否存在
     */
    private static String VNode(Element zroot, String NodeName, Boolean NotBlank) throws Exception {
        Element nownode = zroot.element(NodeName);
        String StPr = nownode.getText().trim();
        if (NotBlank && StrUtil.isBlank(StPr)) {
            throw new Exception(NodeName + " 为空！");
        } else {
            if (nownode.attribute("ConType") != null) {
                if ("dir".equals(nownode.attribute("ConType").getValue())) {
                    File prodir = new File(StPr);
                    if (!prodir.exists()) {
                        throw new Exception(NodeName + "：" + StPr + " 目录不存在！");
                    }
                }
            }
            System.out.println(NodeName + "： " + StPr);
            return StPr;
        }
    }
}
