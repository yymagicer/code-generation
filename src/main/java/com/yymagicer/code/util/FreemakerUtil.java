package com.yymagicer.code.util;

import freemarker.cache.FileTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * freemaker工具类
 *
 * @author yymagicer
 * @version 1.0
 * @date 2021/11/8 下午5:30
 */
public class FreemakerUtil {


     static  Configuration configuration = new Configuration();

    public static Template getTemplateConfig(String ftlPath,String ftlName){

        TemplateLoader templateLoader=null;

        //使用FileTemplateLoader
        try {
            templateLoader=new FileTemplateLoader(new File(ftlPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        configuration.setTemplateLoader(templateLoader);
        Template template = null;
        try {
            template =  configuration.getTemplate(ftlName,"UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return template;
    }



   public  static void write(String ftlPath,String ftlName, String writeFilePath, Map<String,Object> data) throws IOException {
       Template template = getTemplateConfig(ftlPath, ftlName);
       if(null!=template){
           try (Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(writeFilePath), StandardCharsets.UTF_8.name()), 10240)){
               template.process(data, out);
           } catch (FileNotFoundException | UnsupportedEncodingException | TemplateException e) {
               e.printStackTrace();
           }
       }
   }
}
