package com.yymagicer.code.service;

import com.yymagicer.code.entity.CodeContext;
import com.yymagicer.code.entity.TableInfo;
import com.yymagicer.code.util.FreemakerUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author yymagicer
 * @version 1.0
 * @date 2022/1/19 上午1:08
 */
public class BuildCode {

    public static void buildEntity(CodeContext codeContext){
        process(codeContext,"entity.ftl",".java");
    }

    private static  void process(CodeContext codeContext,String ftlName,String generationFileName){
        Map<String,Object> data = new HashMap<>();
        List<TableInfo> tableInfoList = codeContext.getTableInfoList();
        String ftlPath = codeContext.getPathInfo().getFtlPath();
        tableInfoList.forEach(item->{
            data.put("tableInfo",item);
            data.put("pathInfo",codeContext.getPathInfo());
            try {
                FreemakerUtil.write(ftlPath,ftlName,buildFileName(codeContext.getCodePath(),item.getEntityName(),generationFileName),data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private static String buildFileName(String codePath, String entityName, String generationFileName){
        return codePath + "/" + entityName + generationFileName;
    }
    public static void buildMapper(CodeContext codeContext){
        process(codeContext,"mapper.ftl","Mapper.java");
    }
    public static void buildXml(CodeContext codeContext){
        process(codeContext,"xml.ftl","Mapper.xml");
    }
}
