package com.yymagicer.code.entity;

import lombok.Data;

import java.util.List;

/**
 * TODO
 *
 * @author yymagicer
 * @version 1.0
 * @date 2022/1/19 上午12:46
 */
@Data
public class CodeContext {

    private PathInfo pathInfo;

    private List<TableInfo> tableInfoList;

    private String codePath;

}
