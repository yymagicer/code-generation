package com.yymagicer.code.entity;

import lombok.Data;

/**
 * TODO
 *
 * @author yymagicer
 * @version 1.0
 * @date 2022/1/18 下午11:23
 */
@Data
public class PathInfo {

    private String entityPath;

    private String mapperPath;

    private String xmlPath;

    private String servicePath;

    private String serviceImplPath;

    private String ftlPath;

}
