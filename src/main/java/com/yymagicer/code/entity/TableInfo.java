package com.yymagicer.code.entity;

import lombok.Data;

import java.util.List;

/**
 * TODO
 *
 * @author yymagicer
 * @version 1.0
 * @date 2022/1/18 下午11:17
 */
@Data
public class TableInfo {

    private String primaryKeyName;

    private String tableName;

    private String entityName;

    private String tableComment;

    private ColumnInfo primaryColumn;

    private List<ColumnInfo> commonColumns;

    private List<ColumnInfo> allColumns;

}
