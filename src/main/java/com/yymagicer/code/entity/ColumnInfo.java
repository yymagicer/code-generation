package com.yymagicer.code.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * TODO
 *
 * @author yymagicer
 * @version 1.0
 * @date 2022/1/18 下午11:20
 */
@Data
public class ColumnInfo {

    /**
     *
     */
    private String columnName;

    private String fieldName;

    private String columnComment;

    private String fieldType;

    private String fullFieldType;

    private String dbType;

    private String jdbcType;

    public void setType(String type) throws Exception
    {
        switch (type)
        {
            case "tinyint":
                this.jdbcType = "TINYINT";
                this.fieldType = "Byte";
                this.fullFieldType = "java.lang.Byte";
                break;
            case "smallint":
                this.jdbcType = "SMALLINT";
                this.fieldType = "Short";
                this.fullFieldType = "java.lang.Short";
                break;
            case "mediumint":
                this.jdbcType = "INTEGER";
                this.fieldType = "Integer";
                this.fullFieldType = "java.lang.Integer";
                break;
            case "int":
                this.jdbcType = "INTEGER";
                this.fieldType = "Integer";
                this.fullFieldType = "java.lang.Integer";
                break;
            case "integer":
                this.jdbcType = "INTEGER";
                this.fieldType = "Integer";
                this.fullFieldType = "java.lang.Integer";
                break;
            case "bigint":
                this.jdbcType = "BIGINT";
                this.fieldType = "Long";
                this.fullFieldType = "java.lang.Long";
                break;
            case "bit":
                this.jdbcType = "BIT";
                this.fieldType = "Boolean";
                this.fullFieldType = "java.lang.Boolean";
                break;
            case "real":
                this.jdbcType = "DOUBLE";
                this.fieldType = "Double";
                this.fullFieldType = "java.lang.Double";
                break;
            case "double":
                this.jdbcType = "DOUBLE";
                this.fieldType = "Double";
                this.fullFieldType = "java.lang.Double";
                break;
            case "float":
                this.jdbcType = "REAL";
                this.fieldType = "Float";
                this.fullFieldType = "java.lang.Float";
                break;
            case "decimal":
                this.jdbcType = "DECIMAL";
                this.fieldType = "BigDecimal";
                this.fullFieldType = "	java.math.BigDecimal";
                break;
            case "numeric":
                this.jdbcType = "DECIMAL";
                this.fieldType = "BigDecimal";
                this.fullFieldType = "	java.math.BigDecimal";
                break;
            case "char":
                this.jdbcType = "CHAR";
                this.fieldType = "String";
                this.fullFieldType = "java.lang.String";
                break;
            case "varchar":
                this.jdbcType = "VARCHAR";
                this.fieldType = "String";
                this.fullFieldType = "java.lang.String";
                break;
            case "date":
                this.jdbcType = "DATE";
                this.fieldType = "Date";
                this.fullFieldType = "java.util.Date";
                break;
            case "time":
                this.jdbcType = "TIME";
                this.fieldType = "Date";
                this.fullFieldType = "java.util.Date";
                break;
            case "year":
                this.jdbcType = "DATE";
                this.fieldType = "Date";
                this.fullFieldType = "java.util.Date";
                break;
            case "timestamp":
                this.jdbcType = "TIMESTAMP";
                this.fieldType = "Date";
                this.fullFieldType = "java.util.Date";
                break;
            case "datetime":
                this.jdbcType = "TIMESTAMP";
                this.fieldType = "Date";
                this.fullFieldType = "java.util.Date";
                break;
            case "tinytext":
                this.jdbcType = "VARCHAR";
                this.fieldType = "String";
                this.fullFieldType = "java.lang.String";
                break;
            case "tinyblob":
                this.jdbcType = "BINARY";
                this.fieldType = "byte[]";
                this.fullFieldType = "";
                break;
            case "blob":
                this.jdbcType = "LONGVARBINARY";
                this.fieldType = "byte[]";
                this.fullFieldType = "";
                break;
            case "mediumblob":
                this.jdbcType = "LONGVARBINARY";
                this.fieldType = "byte[]";
                this.fullFieldType = "";
                break;
            case "longblob":
                this.jdbcType = "LONGVARBINARY";
                this.fieldType = "byte[]";
                this.fullFieldType = "";
                break;
            case "text":
                this.jdbcType = "LONGVARCHAR";
                this.fieldType = "String";
                this.fullFieldType = "java.lang.String";
                break;
            case "mediumtext":
                this.jdbcType = "LONGVARCHAR";
                this.fieldType = "String";
                this.fullFieldType = "java.lang.String";
                break;
            case "longtext":
                this.jdbcType = "LONGVARCHAR";
                this.fieldType = "String";
                this.fullFieldType = "java.lang.String";
                break;
            default:
                throw new Exception("没有找到 [" + this.jdbcType + "]对应的类型名！");
        }
    }

}
