<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${pathInfo.mapperPath}.${tableInfo.entityName}" >
    <resultMap id="BaseResultMap" type="${pathInfo.entityPath}.${tableInfo.entityName}" >
        <#if tableInfo.primaryColumn ?? >
        <result column="${tableInfo.primaryColumn.columnName}" property="${tableInfo.primaryColumn.fieldName}" jdbcType="${tableInfo.primaryColumn.jdbcType}" />
        </#if>
        <#if tableInfo.commonColumns ?? >
        <#list tableInfo.commonColumns as column>
        <!-- ${column.columnComment} -->
        <result column="${column.columnName}" property="${column.fieldName}" jdbcType="${column.jdbcType}" />
        </#list>
        </#if>
    </resultMap>
    <sql id="Base_Column_List" >
    <#if tableInfo.allColumns ?? >
    <#list tableInfo.allColumns as column>${column.columnName}<#if column_index+1 lt tableInfo.allColumns?size>,</#if></#list>
    </#if>
    </sql>

    <insert id="insert" parameterType="${pathInfo.entityPath}.${tableInfo.entityName}" >
    INSERT INTO ${tableInfo.tableName}
      (<#if tableInfo.allColumns ?? >
           <#list tableInfo.allColumns as column>${column.columnName}<#if column_index+1 lt tableInfo.allColumns?size>,</#if></#list>
       </#if>)
    VALUES (
       <#if tableInfo.allColumns ?? >
          <#list tableInfo.allColumns as column><#noparse>#{</#noparse>${column.columnName}, jdbcType=${column.jdbcType}}<#if column_index+1 lt tableInfo.allColumns?size>,</#if></#list>
       </#if>)
    </insert>

    <insert id="insertSelective" parameterType="${pathInfo.entityPath}.${tableInfo.entityName}" >
        INSERT INTO ${tableInfo.tableName}
        <trim prefix="(" suffix=")" suffixOverrides="," >
            <#if tableInfo.allColumns ?? >
            <#list tableInfo.allColumns as column>
            <if test="${column.fieldName} != null" >
                ${column.columnName},
            </if>
            </#list>
            </#if>
        </trim>
        <trim prefix="VALUES (" suffix=")" suffixOverrides="," >
            <#if tableInfo.allColumns ?? >
            <#list tableInfo.allColumns as column>
            <if test="${column.fieldName} != null" >
                 <#noparse>#{</#noparse>${column.fieldName}, jdbcType=${column.jdbcType}},
            </if>
            </#list>
            </#if>
        </trim>
    </insert>

    <delete id="deleteByPrimaryKey" parameterType="${tableInfo.primaryColumn.fullFieldType}" >
            DELETE FROM ${tableInfo.tableName}
            WHERE ${tableInfo.primaryColumn.columnName} = <#noparse>#{</#noparse>${tableInfo.primaryColumn.fieldName},jdbcType=${tableInfo.primaryColumn.jdbcType}}
    </delete>

    <update id="updateByPrimaryKeySelective" parameterType="${pathInfo.entityPath}.${tableInfo.entityName}" >
            UPDATE ${tableInfo.tableName}
            <set>
            <#if tableInfo.commonColumns ?? >
            <#list tableInfo.commonColumns as column>
            <if test="${column.fieldName} != null" >
                 ${column.columnName} = <#noparse>#{</#noparse>${column.fieldName}, jdbcType=${column.jdbcType}},
            </if>
            </#list>
            </#if>
            </set>
            WHERE  ${tableInfo.primaryColumn.columnName} = <#noparse>#{</#noparse>${tableInfo.primaryColumn.fieldName},jdbcType=${tableInfo.primaryColumn.jdbcType}}
    </update>

    <select id="selectByPrimaryKey" resultType="${pathInfo.entityPath}.${tableInfo.entityName}" parameterType="${tableInfo.primaryColumn.fullFieldType}" >
            SELECT
            <include refid="Base_Column_List" />
            FROM ${tableInfo.tableName}
            WHERE  ${tableInfo.primaryColumn.columnName} = <#noparse>#{</#noparse>${tableInfo.primaryColumn.fieldName},jdbcType=${tableInfo.primaryColumn.jdbcType}}
    </select>
</mapper>%