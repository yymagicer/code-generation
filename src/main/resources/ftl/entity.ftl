package ${pathInfo.entityPath};

import lombok.Data;

/**
 * ${tableInfo.tableComment}
 *
 */
@Data
public class ${tableInfo.entityName} {

    <#if tableInfo.allColumns ??>
    <#list tableInfo.allColumns as column>
     /**
     *  ${column.columnComment}
     */
    private ${column.fieldType} ${column.fieldName};
    </#list>
    </#if>
}