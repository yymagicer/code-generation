package ${pathInfo.mapperPath};

import ${pathInfo.entityPath}.${tableInfo.entityName};


/**
 * ${tableInfo.tableComment} mapper
 *
 */
public interface ${tableInfo.entityName}Mapper extends IBaseMapper<${tableInfo.entityName}> {
}
